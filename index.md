# HEATWAVE MUTUAL-AID

---

# Table of Contents

1. [EVACUATION](#Evacuation)
2. [RESOURCES](#Resources)
    1. [WATER](#Water)
    2. [FOOD](#Food)
    3. [HOUSING](#Housing)
    4. [FIRE](#Fire)
    5. [SELF-DEFENCE](#Self-defence)
    6. [COMMUNITY](#Community)
    7. [TRAVELLING](#Travelling)
    8. [MEDIA](#media)
3. [SECURITY-CULTURES](#Security-Cultures)
4. [OTHERS](#Others)

## EVACUATION <a name="Evacuation"></a>

![alt text](https://i.redd.it/e68fr1epq9971.jpg "HOW TO PREPARE FOR EVACUATION")

## RESOURCES <a name="Resources"></a>

### WATER <a name="Water"></a>

![alt text](https://i.redd.it/nq7e2ybl3oj61.png "WATER PURIFICATION and STORAGE")


